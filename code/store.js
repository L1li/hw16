import { modalShow, modalHide, createElement } from "./functions.js";

const tbody = document.querySelector("tbody");
const store = JSON.parse(localStorage.BDStore),
    btnCloseModal = document.getElementById("close"),
    btnSaveModal = document.getElementById("save"),
    editElement = document.querySelector('.edit-element')

btnCloseModal.addEventListener("click", modalHide);

//global function
function editCard(obj={}, typeStore) {
    if (typeStore.indexOf('store') !== -1) {
        editElement.dataset.key = obj.id;
        editElement.innerHTML = '';
        const {keywords, porductPrice, productDescription, productImage, productName, productQuantity, status} = obj;
        const inputs = [
            createElement('input', undefined, keywords.join(), "Ключові слова"),
            createElement('input', undefined, porductPrice, "Ціна", 'number'),
            createElement('input', undefined, productDescription, "Опис"),
            createElement('input', undefined, productImage, "Посилання на зображення"),
            createElement('input', undefined, productName, "Назва"),
            createElement('input', undefined, productQuantity, "Кількість", 'number'),
            createElement('input', undefined, status, "Статус", 'checkbox')
        ]
        inputs.forEach((el) => {
            console.log(el)
        })
        editElement.append(...inputs);
    }
 
}
//global function end



function createElementTable(store) {
    return store.map((el, i) => {
        return `
        <tr>
            <td>${i + 1}</td>
            <td>${el.productName}</td>
            <td title="При настиску сортувати.">${el.productQuantity}</td>
            <td title="При настиску сортувати.">${el.porductPrice} грн.</td>
            <td class="edit" data-index='${i}'>&#128397;</td>
            <td>${el.status ? "&#9989;" : "&#10060;"}</td>
            <td>${el.date}</td>
            <td>&#128465;</td>
        </tr>
        `
    }).join("")
}

tbody.insertAdjacentHTML("beforeend", createElementTable(store))

const [...edits] = document.querySelectorAll('.edit');

edits.forEach((el) => {
    el.addEventListener('click', (e) => {
        modalShow();
        editCard(store[e.target.dataset.index], document.location.pathname)
    })
})

btnSaveModal.addEventListener("click", () => {
    const inputs = document.querySelectorAll(".edit-element input");

console.log(inputs);
console.log(editElement.dataset.key)
})



