import { modalHide, modalShow, createInputsForModal, generationID, validate, fillData, fillVideo } from "./functions.js";

const btnShowMadal = document.querySelector(".add"),
    btnCloseModal = document.getElementById("close"),
    btnSaveModal = document.getElementById("save"),
    select = document.getElementById("select"),
    formInfo = document.querySelector(".form-info"),
    store = ["Назва продукту", "Вартість продукту", "Посилання на зображення", "Опис продукту", "Ключеві слова (Розділяти комою)"],
    menu = ["Назва cтрави", "Вага", "Інгредієнти", "Вага продуктів", "Посилання на зображення", "Вартість", "Ключеві слова (Розділяти комою)"],
    video = ["Назва відео", "Жанр", "Країна походження", "Рік", "Посилання на відео", "Актори", 'Опис відео'],
    objStore = {
        id: "",
        status: false,
        productName: "",
        porductPrice: 0,
        productImage: "",
        productDescription: "",
        productQuantity: 0,
        keywords: []
    },
    restorationObj = {
        id: '',
        productName: "",
        productWeiht: "",
        ingredients: "",
        price: 0,
        productImageUrl: "",
        keywords: [],
        weiht: 0,
        stopList: false,
        like: 0,
        ageRestrictions: false
    }, 
    videoObj = {
        id: '',
        videoLink: "",//посилання на відео
        videoName: "",
        videoGenre: [],
        videoCountry: [],
        videoActors: [],
        videoDescription: "",
        videoYearReleased: 2023
    }

let typeCategory = null;


btnShowMadal.addEventListener("click", modalShow);
btnCloseModal.addEventListener("click", modalHide);

select.addEventListener("change", () => {
    typeCategory = select.value;

    if (select.value === "Магазин") {
        formInfo.innerHTML = '';
        formInfo.insertAdjacentHTML("beforeend", createInputsForModal(store));
    } else if (select.value === "Відео хостинг") {
        formInfo.innerHTML = '';
        formInfo.insertAdjacentHTML("beforeend", createInputsForModal(video));
    } else if (select.value === "Ресторан") {
        formInfo.innerHTML = '';
        formInfo.insertAdjacentHTML("beforeend", createInputsForModal(menu));
    } else {
        console.error("Жоден з пунктів не валідний.")
        return
    }
})

btnSaveModal.addEventListener("click", () => {
    const [...inputs] = document.querySelectorAll(".form-info input");
    if (typeCategory === "Магазин") {
        objStore.id = generationID();
        inputs.forEach((input) => {
            if (input.value.length > 3) {
                if (input.dataset.type === "Назва продукту") {
                    objStore.productName = input.value;
                    input.value = '';
                } else if (input.dataset.type === "Вартість продукту") {
                    objStore.porductPrice = parseFloat(input.value);
                    input.value = '';
                } else if (input.dataset.type === "Посилання на зображення") {
                    objStore.productImage = input.value;
                    input.value = '';
                } else if (input.dataset.type === "Опис продукту") {
                    objStore.productDescription = input.value;
                    input.value = '';
                } else if (input.dataset.type === "Ключеві слова (Розділяти комою)") {
                    objStore.keywords.push(...input.value.split(","));
                    input.value = '';
                }
                input.classList.remove("error");
            } else {
                input.classList.add("error");
                return
            }
        })
        objStore.date = new Date();
        if (objStore.productQuantity <= 0) {
            objStore.status = false;
        } else {
            objStore.status = true;
        }

        if (objStore.productName !== "" && objStore.porductPrice != 0 && validate(/\d/, objStore.porductPrice) && objStore.productImage != "" && objStore.productDescription != "" && objStore.keywords.length != 0) {
            const store = JSON.parse(localStorage.BDStore);
            store.push(objStore);
            localStorage.BDStore = JSON.stringify(store);
        }
    } else if (typeCategory === "Ресторан") {
        restorationObj.id = generationID();
        fillData(inputs, restorationObj)

        if (restorationObj.productName !== "" && restorationObj.productWeiht !== "" && restorationObj.ingredients !== "" && restorationObj.price != 0 && validate(/\d/, restorationObj.price) && restorationObj.productImageUrl != "" && restorationObj.weiht != 0 && validate(/\d/, restorationObj.weiht) && restorationObj.keywords.length != 0) {
            const restorant = JSON.parse(localStorage.BDRestorant);
            restorant.push(restorationObj);
            localStorage.BDRestorant = JSON.stringify(restorant);
        }
    } else if (typeCategory === "Відео хостинг") {
        videoObj.id = generationID();
        fillVideo(inputs, videoObj)
        console.dir(videoObj)
        if (videoObj.videoName !== "" && videoObj.videoYearReleased !== 0 && validate(/\d/, videoObj.videoYearReleased) && videoObj.videoGenre.length != 0 && videoObj.videoCountry.length != 0 && videoObj.videoActors.length != 0 && videoObj.videoLink != "" && videoObj.videoDescription != "") {
            const video = JSON.parse(localStorage.BDVideo);
            video.push(videoObj);
            localStorage.BDVideo = JSON.stringify(video);
        }
    }
})






