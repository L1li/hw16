export function isAuthorization() {
    if (document.location.pathname.search("authorization") !== -1) {
        return
    }
    if (!localStorage.isAuthorization) {
        document.location = "/authorization"
    }
}

export function validate (rex, val) {
    return rex.test(val)
}

const containerModal = document.querySelector(".container-modal");

export const modalShow = e => containerModal.classList.add("active");
export const modalHide = e => containerModal.classList.remove("active");

export function createInputsForModal(arr = [], info2) {
    return arr.map((input) => {
        const id = generationID();
        return `<div class="input-line"><label for="${id}">${input}</label><input data-type="${input}" type="text" id="${id}"></div>`
    }).join("")
}

export function generationID() {
    const a = ['q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'z', 'x', 'c', 'v', 'b', 'n', 'm', 'й', 'ц', 'у', 'к', 'е', 'н', 'г', 'ш', 'щ', 'з', 'х', 'ї', 'ф', 'і', 'в', 'а', 'п', 'р', 'о', 'л', 'д', 'ж', 'є', 'я', 'ч', 'с', 'м', 'и', 'т', 'ь', 'б', 'ю'];
    const b = [1, 2, 3, 4, 5, 6, 7, 8, 9];
    let id = "";
    for (let i = 0; i < 10; i++) {
        id += `${a[Math.floor(Math.random() * a.length)]}${b[Math.floor(Math.random() * b.length)]}`
    }
    return id
}

export const fillData = (inputs, obj) => {
    inputs.forEach((input) => {
        if (input.value.length > 3) {
            if (input.dataset.type === "Назва cтрави") {
                obj.productName = input.value;
                input.value = '';
            } else if (input.dataset.type === "Вага") {
                obj.weiht = parseFloat(input.value);
                input.value = '';
            } else if (input.dataset.type === "Інгредієнти") {
                obj.ingredients = input.value;
                input.value = '';
            } else if (input.dataset.type === "Вага продуктів") {
                obj.productWeiht = input.value;
                input.value = '';
            } else if (input.dataset.type === "Посилання на зображення") {
                obj.productImageUrl = input.value;
                input.value = '';
            } else if (input.dataset.type === "Вартість") {
                obj.price = parseFloat(input.value);
                input.value = '';
            } else if (input.dataset.type === "Ключеві слова (Розділяти комою)") {
                obj.keywords.push(...input.value.split(","));
                input.value = '';
            }
            input.classList.remove("error");
        } else {
            input.classList.add("error");
            return
        }
    })
    obj.date = new Date();
}

export const fillVideo = (inputs, obj) => {
    inputs.forEach((input) => {
        if (input.value.length > 3) {
            if (input.dataset.type === "Назва відео") {
                obj.videoName = input.value;
                input.value = '';
            } else if (input.dataset.type === "Рік") {
                obj.videoYearReleased = parseFloat(input.value);
                input.value = '';
            } else if (input.dataset.type === "Жанр") {
                obj.videoGenre.push(...input.value.split(","));
                input.value = '';
            } else if (input.dataset.type === "Країна походження") {
                obj.videoCountry.push(...input.value.split(","));
                input.value = '';
            } else if (input.dataset.type === "Посилання на відео") {
                obj.videoLink = input.value;
                input.value = '';
            } else if (input.dataset.type === "Опис відео") {
                obj.videoDescription = input.value;
                input.value = '';
            } else if (input.dataset.type === "Актори") {
                obj.videoActors.push(...input.value.split(","));
                input.value = '';
            }
            input.classList.remove("error");
        } else {
            input.classList.add("error");
            return
        }
    })
    obj.date = new Date();
}

export const createElement = (el = 'div', cl = "", value = "", placehold = '', type = 'text') => {
    const element = document.createElement(el);
    if (cl !=="") {
        element.classList.add(cl);
    }
    if (element.tagName === 'INPUT') {
        element.value = value;
        element.type = type;
        if (element.type === 'checkbox') {
            element.checked = value;
        }
        const label = document.createElement('label'),
        id = generationID(),
        div = document.createElement('div')
        label.innerText = placehold;
        label.setAttribute('for', id);
        element.id = id;
        label.append(element);
        div.append(label);
        return div;
    } else {
        return element.innerText = value;
    }


}
