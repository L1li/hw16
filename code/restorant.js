const tbody = document.querySelector("tbody");
const restorant = JSON.parse(localStorage.BDRestorant);


function createElementTable (store) {
   return store.map((el, i) => {
        return `
        <tr>
            <td>${i + 1}</td>
            <td>${el.productName}</td>
            <td title="При настиску сортувати.">${el.weiht} г</td>
            <td title="При настиску сортувати.">${el.price} грн.</td>
            <td>&#128397;</td>
            <td>${el.stopList ? "&#9989;" : "&#10060;" }</td>
            <td>${el.date}</td>
            <td>&#128465;</td>
        </tr>
        `
    }).join("")
}

tbody.insertAdjacentHTML("beforeend", createElementTable(restorant))



